# Latex Hierarchy Data Tree 

A sample code for LaTeX to create Hierarchy Data Tree using tikz package.

The document class used here is **beamer** for Slide presentation with `Malmoe` theme

### Used Packages

The packages used for this sample LaTeX project are:

    1. inputenc
    2. fontenc
    3. tikz

    
The sample image of the Hierarchy Data Tree is shown below:

![Hierarchy Data Tree](img/hdata.png "Hierarchy Data Tree created using Tikz Package LaTeX")

### License

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.